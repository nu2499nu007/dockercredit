﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DockerCredit.Models
{
    //Class custom เอง
    public class Member
    {
        public int id { get; set; }
        public string sFirstName { get; set; }
        public string sLastName { get; set; }
        public int nAge { get; set; }
        public string sAddress { get; set; }
        public string sProvice { get; set; }
        public string sPostCode { get; set; }
        [DataType(DataType.Date)]
        public DateTime BirdDay { get; set; }
    }
}
