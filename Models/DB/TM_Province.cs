﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DockerCredit.Models.DB
{
    public partial class TM_Province
    {
        public string sCode { get; set; }
        public string sRegionID { get; set; }
        public string sName { get; set; }
        public string sShort { get; set; }
        public string sNameEN { get; set; }
    }
}
