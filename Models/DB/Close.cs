﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DockerCredit.Models.DB
{
    public partial class Close
    {
        public string Cl_id { get; set; }
        public string M_id { get; set; }
        public string E_id { get; set; }
        public string Cl_date { get; set; }
        public string Cl_Cause { get; set; }
    }
}
