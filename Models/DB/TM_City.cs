﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DockerCredit.Models.DB
{
    public partial class TM_City
    {
        public TM_City()
        {
            TM_Districts = new HashSet<TM_District>();
        }

        public string sCode { get; set; }
        public string sCode_Province { get; set; }
        public string sName { get; set; }
        public string sPostCode { get; set; }
        public string sNameEN { get; set; }

        public virtual ICollection<TM_District> TM_Districts { get; set; }
    }
}
