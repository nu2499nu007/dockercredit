﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DockerCredit.Models.DB
{
    public partial class Value
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
