﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DockerCredit.Models.DB
{
    public partial class P_Money
    {
        public int P_id { get; set; }
        public string P_name { get; set; }
        public string P_Card { get; set; }
        public string P_Tel { get; set; }
        public string P_Add { get; set; }
        public string P_Sex { get; set; }
    }
}
