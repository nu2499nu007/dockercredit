﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DockerCredit.Models.DB
{
    public partial class D_Del
    {
        public int D_id { get; set; }
        public string M_id { get; set; }
        public string Bo_id { get; set; }
        public string E_id { get; set; }
        public string D_num { get; set; }
        public byte[] D_date { get; set; }
    }
}
