﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DockerCredit.Models.DB
{
    public partial class Pay
    {
        public string Pay_id { get; set; }
        public string M_id { get; set; }
        public string E_id { get; set; }
        public byte[] Pay_date { get; set; }
        public float? Pay_net { get; set; }
    }
}
