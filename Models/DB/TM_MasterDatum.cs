﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DockerCredit.Models.DB
{
    public partial class TM_MasterDatum
    {
        public int nID { get; set; }
        public string sDataName { get; set; }
        public string sAbbr { get; set; }
        public string sType { get; set; }
        public decimal? nOrder { get; set; }
        public bool isActive { get; set; }
        public bool isDel { get; set; }
    }
}
