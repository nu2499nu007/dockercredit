﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DockerCredit.Models.DB
{
    public partial class Ch
    {
        public int Ch_id { get; set; }
        public string G_id { get; set; }
        public string M_id { get; set; }
        public string E_id { get; set; }
        public byte[] Ch_date { get; set; }
        public float? Ch_count { get; set; }
    }
}
