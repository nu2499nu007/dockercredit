﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DockerCredit.Models.DB
{
    public partial class TM_District
    {
        public string sCode { get; set; }
        public string sCode_City { get; set; }
        public string sName { get; set; }
        public string sNameEN { get; set; }

        public virtual TM_City sCode_CityNavigation { get; set; }
    }
}
