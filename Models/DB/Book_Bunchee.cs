﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DockerCredit.Models.DB
{
    public partial class Book_Bunchee
    {
        public int Bo_id { get; set; }
        public string M_id { get; set; }
        public string Bo_name { get; set; }
    }
}
