﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DockerCredit.Models.DB
{
    public partial class C_Moneyy
    {
        public int C_id { get; set; }
        public string M_id { get; set; }
        public string Bo_id { get; set; }
        public string E_id { get; set; }
        public string C_num { get; set; }
        public byte[] C_date { get; set; }
    }
}
