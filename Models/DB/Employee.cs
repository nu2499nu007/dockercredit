﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DockerCredit.Models.DB
{
    public partial class Employee
    {
        public int E_Id { get; set; }
        public string E_Name { get; set; }
        public string E_Address { get; set; }
        public int? E_Tel { get; set; }
        public string E_Username { get; set; }
        public string E_Password { get; set; }
        public bool IsActive { get; set; }
        public bool IsDel { get; set; }
        public DateTime? dCreate { get; set; }
        public string sCreate { get; set; }
        public DateTime? dUpdate { get; set; }
        public string sUpdate { get; set; }
    }
}
