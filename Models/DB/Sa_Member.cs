﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DockerCredit.Models.DB
{
    public partial class Sa_Member
    {
        public int Sa_id { get; set; }
        public string M_id { get; set; }
        public byte[] Sa_date { get; set; }
    }
}
