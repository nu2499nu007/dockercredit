﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DockerCredit.Models.DB
{
    public partial class Bill_Money
    {
        public int Bm_id { get; set; }
        public string C_id { get; set; }
        public string M_id { get; set; }
    }
}
