﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace DockerCredit.Models.DB
{
    public partial class CreditEntity : DbContext
    {
        public CreditEntity()
        {
        }

        public CreditEntity(DbContextOptions<CreditEntity> options)
            : base(options)
        {
        }

        public virtual DbSet<Bill_Money> Bill_Moneys { get; set; }
        public virtual DbSet<Book_Bunchee> Book_Bunchees { get; set; }
        public virtual DbSet<C_G> C_Gs { get; set; }
        public virtual DbSet<C_Moneyy> C_Moneyys { get; set; }
        public virtual DbSet<Ch> Ches { get; set; }
        public virtual DbSet<Close> Closes { get; set; }
        public virtual DbSet<Cut_Money> Cut_Moneys { get; set; }
        public virtual DbSet<D_Del> D_Dels { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Member> Members { get; set; }
        public virtual DbSet<P_Money> P_Moneys { get; set; }
        public virtual DbSet<Pay> Pays { get; set; }
        public virtual DbSet<Sa_Member> Sa_Members { get; set; }
        public virtual DbSet<Slip> Slips { get; set; }
        public virtual DbSet<TM_City> TM_Cities { get; set; }
        public virtual DbSet<TM_District> TM_Districts { get; set; }
        public virtual DbSet<TM_MasterDatum> TM_MasterData { get; set; }
        public virtual DbSet<TM_Province> TM_Provinces { get; set; }
        public virtual DbSet<TMenu> TMenus { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Value> Values { get; set; }
        public virtual DbSet<Z_Money> Z_Moneys { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=localhost;database=CreditUnion;User ID=sa;Password=dewis@1234;Persist Security Info=False;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Thai_CS_AI");

            modelBuilder.Entity<Bill_Money>(entity =>
            {
                entity.HasKey(e => e.Bm_id)
                    .HasName("PK_BILL_MONEY");

                entity.ToTable("Bill_Money");

                entity.Property(e => e.Bm_id).ValueGeneratedNever();

                entity.Property(e => e.C_id)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.M_id)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Book_Bunchee>(entity =>
            {
                entity.HasKey(e => e.Bo_id)
                    .HasName("PK_BOOK_BUNCHEE");

                entity.ToTable("Book_Bunchee");

                entity.Property(e => e.Bo_id).ValueGeneratedNever();

                entity.Property(e => e.Bo_name)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.M_id)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<C_G>(entity =>
            {
                entity.HasKey(e => e.C_G_id);

                entity.ToTable("C_G");

                entity.Property(e => e.C_G_id).ValueGeneratedNever();

                entity.Property(e => e.E_id)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.G_id)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.M_id)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.P_id)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<C_Moneyy>(entity =>
            {
                entity.HasKey(e => e.C_id)
                    .HasName("PK_C_MONEYY");

                entity.ToTable("C_Moneyy");

                entity.Property(e => e.C_id).ValueGeneratedNever();

                entity.Property(e => e.Bo_id)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.C_date)
                    .IsRowVersion()
                    .IsConcurrencyToken();

                entity.Property(e => e.C_num)
                    .HasMaxLength(7)
                    .IsUnicode(false);

                entity.Property(e => e.E_id)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.M_id)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Ch>(entity =>
            {
                entity.HasKey(e => e.Ch_id)
                    .HasName("PK_CH");

                entity.ToTable("Ch");

                entity.Property(e => e.Ch_id).ValueGeneratedNever();

                entity.Property(e => e.Ch_date)
                    .IsRowVersion()
                    .IsConcurrencyToken();

                entity.Property(e => e.E_id)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.G_id)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.M_id)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Close>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Close");

                entity.Property(e => e.Cl_Cause)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.Cl_date)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.Cl_id)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.E_id)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.M_id)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength(true);
            });

            modelBuilder.Entity<Cut_Money>(entity =>
            {
                entity.HasKey(e => e.Cut_id)
                    .HasName("PK_CUT_MONEY");

                entity.ToTable("Cut_Money");

                entity.Property(e => e.Cut_id).ValueGeneratedNever();

                entity.Property(e => e.C_id)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.M_id)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<D_Del>(entity =>
            {
                entity.HasKey(e => e.D_id)
                    .HasName("PK_D_DEL");

                entity.ToTable("D_Del");

                entity.Property(e => e.D_id).ValueGeneratedNever();

                entity.Property(e => e.Bo_id)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.D_date)
                    .IsRowVersion()
                    .IsConcurrencyToken();

                entity.Property(e => e.D_num)
                    .HasMaxLength(7)
                    .IsUnicode(false);

                entity.Property(e => e.E_id)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.M_id)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.HasKey(e => e.E_Id);

                entity.ToTable("Employee");

                entity.Property(e => e.E_Address)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasComment("ที่อยู่");

                entity.Property(e => e.E_Name)
                    .HasMaxLength(150)
                    .IsUnicode(false)
                    .HasComment("ชื่อเจ้าหน้าที่");

                entity.Property(e => e.E_Password)
                    .HasMaxLength(150)
                    .IsUnicode(false)
                    .HasComment("password");

                entity.Property(e => e.E_Tel).HasComment("เบอร์โทร");

                entity.Property(e => e.E_Username)
                    .HasMaxLength(150)
                    .IsUnicode(false)
                    .HasComment("ชื่อผู้ใช้งาน");

                entity.Property(e => e.dCreate).HasColumnType("datetime");

                entity.Property(e => e.dUpdate).HasColumnType("datetime");

                entity.Property(e => e.sCreate)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.sUpdate)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Member>(entity =>
            {
                entity.HasKey(e => e.nUserID);

                entity.ToTable("Member");

                entity.Property(e => e.nUserID).ValueGeneratedNever();

                entity.Property(e => e.DateOfBirth)
                    .HasColumnType("datetime")
                    .HasComment("วันเกิด");

                entity.Property(e => e.M_ID)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.cActive).HasComment("ใช้งาน");

                entity.Property(e => e.cDel).HasComment("1 = ลบ 0 = ไม่ลบ");

                entity.Property(e => e.dCreate)
                    .HasColumnType("datetime")
                    .HasComment("วันที่สร้างรายการ");

                entity.Property(e => e.dUpdate)
                    .HasColumnType("datetime")
                    .HasComment("วันที่Update");

                entity.Property(e => e.mem_Title)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasComment("ชื่อนำหน้า");

                entity.Property(e => e.sAddress)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasComment("ที่อยู่");

                entity.Property(e => e.sCreateBy).HasComment("ผู้สร้างรายการ");

                entity.Property(e => e.sDateofBegin)
                    .HasColumnType("datetime")
                    .HasComment("วันที่สมัคร");

                entity.Property(e => e.sFName)
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasComment("ชื่อ");

                entity.Property(e => e.sID_Card)
                    .HasMaxLength(13)
                    .IsUnicode(false)
                    .HasComment("บัตรประชาชน");

                entity.Property(e => e.sLName)
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasComment("นามสกุล");

                entity.Property(e => e.sPhone)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasComment("เบอร์โทร");

                entity.Property(e => e.sPhoto)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasComment("รูป");

                entity.Property(e => e.sPost)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasComment("ไปรษณีย์");

                entity.Property(e => e.sRemark)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasComment("หมายเหตุ");

                entity.Property(e => e.sTo)
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasComment("ทายาท");

                entity.Property(e => e.sUpdateBy).HasComment("ผู้อัพเดพรายการ");
            });

            modelBuilder.Entity<P_Money>(entity =>
            {
                entity.HasKey(e => e.P_id)
                    .HasName("PK_P_MONEY");

                entity.ToTable("P_Money");

                entity.Property(e => e.P_id).ValueGeneratedNever();

                entity.Property(e => e.P_Add)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.P_Card)
                    .HasMaxLength(13)
                    .IsUnicode(false);

                entity.Property(e => e.P_Sex)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.P_Tel)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.P_name)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Pay>(entity =>
            {
                entity.HasKey(e => e.Pay_id)
                    .HasName("PK_PAY");

                entity.ToTable("Pay");

                entity.Property(e => e.Pay_id)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.E_id)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.M_id)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Pay_date)
                    .IsRowVersion()
                    .IsConcurrencyToken();
            });

            modelBuilder.Entity<Sa_Member>(entity =>
            {
                entity.HasKey(e => e.Sa_id)
                    .HasName("PK_SA_MEMBER");

                entity.ToTable("Sa_Member");

                entity.Property(e => e.Sa_id).ValueGeneratedNever();

                entity.Property(e => e.M_id)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Sa_date)
                    .IsRowVersion()
                    .IsConcurrencyToken();
            });

            modelBuilder.Entity<Slip>(entity =>
            {
                entity.HasKey(e => e.Sl_id)
                    .HasName("PK_SLIP");

                entity.ToTable("Slip");

                entity.Property(e => e.Sl_id)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Ch_id)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.E_id)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TM_City>(entity =>
            {
                entity.HasKey(e => e.sCode)
                    .HasName("PK_TAAMPHUR");

                entity.ToTable("TM_City");

                entity.Property(e => e.sCode)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.sCode_Province)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.sName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.sNameEN)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.sPostCode)
                    .HasMaxLength(5)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TM_District>(entity =>
            {
                entity.HasKey(e => e.sCode)
                    .HasName("PK_TATUMBON");

                entity.ToTable("TM_District");

                entity.Property(e => e.sCode)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.sCode_City)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.sName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.sNameEN)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.sCode_CityNavigation)
                    .WithMany(p => p.TM_Districts)
                    .HasForeignKey(d => d.sCode_City)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TATumbon_TAAmphur");
            });

            modelBuilder.Entity<TM_MasterDatum>(entity =>
            {
                entity.HasKey(e => e.nID);

                entity.Property(e => e.nOrder).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.sAbbr)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.sDataName)
                    .HasMaxLength(350)
                    .IsUnicode(false)
                    .HasComment("ชื่อรายการ");

                entity.Property(e => e.sType)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasComment("ประเภทรายการ");
            });

            modelBuilder.Entity<TM_Province>(entity =>
            {
                entity.HasKey(e => e.sCode)
                    .HasName("PK_TAPROVINCE");

                entity.ToTable("TM_Province");

                entity.Property(e => e.sCode)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.sName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.sNameEN)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.sRegionID)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.sShort)
                    .HasMaxLength(6)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TMenu>(entity =>
            {
                entity.HasKey(e => e.nMenuID)
                    .HasName("PK_TMenu_1");

                entity.ToTable("TMenu");

                entity.Property(e => e.nMenuID)
                    .ValueGeneratedNever()
                    .HasComment("รหัสเมนู");

                entity.Property(e => e.cActive).HasComment("สถานะการใช้งาน");

                entity.Property(e => e.sLinkNavigat)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasComment("Icon");

                entity.Property(e => e.sMenuHeadID).HasComment("เมนูหลัก");

                entity.Property(e => e.sMenuLink)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasComment("เมนูลิงค์");

                entity.Property(e => e.sMenuName)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("ชื่อเมนู");

                entity.Property(e => e.sMenuNameTH)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("ชื่อเมนูไทย");

                entity.Property(e => e.sMenuOrder).HasComment("ลำดับการแสดงผลเมนู");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("User");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.PasswordHash).IsUnicode(false);

                entity.Property(e => e.PasswordSalt).IsUnicode(false);

                entity.Property(e => e.Username)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.sName)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Value>(entity =>
            {
                entity.Property(e => e.id).ValueGeneratedNever();

                entity.Property(e => e.name)
                    .HasMaxLength(150)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Z_Money>(entity =>
            {
                entity.HasKey(e => e.Z_id)
                    .HasName("PK_Z_MONEY");

                entity.ToTable("Z_Money");

                entity.Property(e => e.Z_id).ValueGeneratedNever();

                entity.Property(e => e.Z_name)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
