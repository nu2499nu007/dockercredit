﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DockerCredit.Models.DB
{
    public partial class Z_Money
    {
        public int Z_id { get; set; }
        public string Z_name { get; set; }
    }
}
