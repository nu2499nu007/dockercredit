﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DockerCredit.Models.DB
{
    public partial class Cut_Money
    {
        public int Cut_id { get; set; }
        public string C_id { get; set; }
        public string M_id { get; set; }
    }
}
