﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DockerCredit.Models.DB
{
    public partial class C_G
    {
        public int C_G_id { get; set; }
        public string G_id { get; set; }
        public string M_id { get; set; }
        public string E_id { get; set; }
        public string P_id { get; set; }
    }
}
