﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DockerCredit.Models.DB
{
    public partial class TMenu
    {
        public int nMenuID { get; set; }
        public int? sMenuOrder { get; set; }
        public string sMenuName { get; set; }
        public string sMenuNameTH { get; set; }
        public int? sMenuHeadID { get; set; }
        public string sMenuLink { get; set; }
        public string sLinkNavigat { get; set; }
        public bool cActive { get; set; }
    }
}
