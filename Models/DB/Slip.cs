﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DockerCredit.Models.DB
{
    public partial class Slip
    {
        public string Sl_id { get; set; }
        public string Ch_id { get; set; }
        public string E_id { get; set; }
    }
}
