﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DockerCredit.Models.DB
{
    public partial class Member
    {
        public int nUserID { get; set; }
        public string M_ID { get; set; }
        public string mem_Title { get; set; }
        public string sFName { get; set; }
        public string sLName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string sAddress { get; set; }
        public string sPost { get; set; }
        public string sPhone { get; set; }
        public string sTo { get; set; }
        public string sRemark { get; set; }
        public string sID_Card { get; set; }
        public string sPhoto { get; set; }
        public DateTime? sDateofBegin { get; set; }
        public bool cActive { get; set; }
        public bool cDel { get; set; }
        public int? sCreateBy { get; set; }
        public DateTime? dCreate { get; set; }
        public int? sUpdateBy { get; set; }
        public DateTime? dUpdate { get; set; }
    }
}
